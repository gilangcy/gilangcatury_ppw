from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import *
from .models import *

# Create your tests here.
class story7_testPath(TestCase):
    def test_path_story7(self):
        response = Client().get('/story7/',{},True)
        self.assertEquals(response.status_code,200)

   

class story7_testInput(TestCase):
    def test_input_story7(self):
        response = Client().get('/story7/')
        html = response.content.decode('utf-8')
        self.assertIn('Tentang Saya',html)
        self.assertIn('Aktivitas saat ini',html)
        self.assertIn('Pengalaman Organisasi dan Kepanitiaan',html)
        self.assertIn('Prestasi',html)
   
  
class story7_testFunction(TestCase):
    def test_url_using_func(self):
        found = resolve('/story7/')
        self.assertEqual(found.func, accord)


class story7_testTemplate(TestCase):
    def test_form_template_used1(self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response, 'accord.html')
  
    