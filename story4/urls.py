from django.urls import path
from . import views
#from story5 import urls
#from story1 import urls
from story6 import urls


app_name = 'story4'

urlpatterns = [
    path('story4', views.index, name='story4'),
    path('experience/', views.experience, name='experience'),
    path('achievement/', views.achievement, name='achievement'),
    path('volun/', views.volun, name='volun'),
    path('skills/', views.skills, name='skills'),
    path('about/', views.about, name='about'),
     path('edu/', views.edu, name='edu'),
     #path('story5',urls.views.lihatMatkul, name='story5'),
     #path('story1',urls.views.story1 , name="story1"),
     #path('story5',urls.views.pageMatkul, name="story5")
]
