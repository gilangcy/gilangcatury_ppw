from django.shortcuts import render, redirect



# Create your views here.
def index(request):
    return render(request, 'index.html')

def experience(request):
    return render(request, "experience.html")

def achievement(request):
    return render(request, "achievement.html")

def skills(request):
    return render(request, "skills.html")

def volun(request):
    return render(request, "volun.html")

def about(request):
    return render(request, "about.html")

def edu(request):
    return render(request, "edu.html")
