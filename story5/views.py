from django.shortcuts import get_object_or_404, redirect, render
from .models import mkModel
from .forms import mkForm

# Create your views here

def lihatMatkul(request):
    matkul = mkModel.objects.all()

    context = {'page_title':'List Mata Kuliah',
    'matkul':matkul
    }   

    return render(request,'lihatMatkul.html',context)

def detail(request,cek_id):
    context ={'page_title':'Detail Mata Kuliah',
     'cek':cek_id}

    return render(request,'detail.html',context)

def createMatkul(request):
    mataKuliah_form = mkForm(request.POST or None)
    if request.method == 'POST':
        if mataKuliah_form.is_valid():
            mataKuliah_form.save()
            status ={"status":"Data Berhasil ditambahkan","stat":"2"}
            return render(request,'pageMatkul.html',status)

    context ={
            'page_title':'Tambah Matkul',
            'matkul_form': mataKuliah_form,'stat':'1'
    }
    return render(request,'pageMatkul.html',context)

def deleteMatkul(request,delete_id):
    mkModel.objects.filter(id=delete_id).delete()
    return redirect ('story5:lihatMatkul')




