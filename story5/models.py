
from django.db import models
# Create your models here.
from django.forms import ModelForm


# Create your models here.
class mkModel(models.Model) :
	LIST_MATKUL = [
		('Dasar-Dasar Pemrograman','Dasar-Dasar Pemrograman'),
		('Struktur Data dan Algoritma','Struktur Data dan Algoritma'),
		('Pengantar Organisasi Komputer','Pengantar Organisasi Komputer'),
		('Perancangan dan Pemrograman Web','Perancangan dan Pemrograman Web'),
		('Matematika Dasar','Matematika Dasar'),
		('Matematika Diskret','Matematika Diskret')
	]
	LIST_DOSEN = [
		('Gilang,S.Kom.,M.Kom.','Gilang,S.Kom.,M.Kom.'),
		('Dr.Yudishtira M.CompSc.','Dr.Yudishtira M.CompSc.'),
		('Catur, S.Kom.,M.Kom.','Catur, S.Kom.,M.Kom.'),
		('Cey,M.CompSc','Cey,M.CompSc'),
		('Prof.Geceye,M.CompSc.','Prof.Geceye,M.CompSc.'),
		('Ir.Lang.M.Si.','Ir.Lang.M.Si.')
	]
	LIST_JAM =[
		('Senin 08.00-10.00','Senin 08.00-10.00'),
		('Senin 10.00-12.00','Senin 10.00-12.00'),
		('Selasa 08.00-10.00','Selasa 08.00-10.00'),
		('Selasa 10.00-12.00','Selasa 10.00-12.00'),
	]
	LIST_Ruangan =[
		('2.301','2.301'),
		('2.302','2.302'),
		('2.303','2.303'),
		('2.304','2.304'),
		('2.401','2.401'),
		('2.402','2.402'),
		('2.403','2.403'),
		('2.404','2.404')
	]

	nama = models.CharField(
					max_length=100 , 
					choices = LIST_MATKUL, 
					default='Dasar-Dasar Pemrograman')
	Dosen =  models.CharField(
					max_length=100 , 
					choices = LIST_DOSEN, 
					default='Gilang,S.Kom.,M.Kom.')
	jam =  models.CharField(
					max_length=100 , 
					choices = LIST_JAM, 
					default='Selasa 10.00-12.00')
	ruangan = models.CharField(
		max_length=100, choices = LIST_Ruangan, default='2.304'
	)
		
	
	def __str__(self):
			return "{}".format(self.nama)




# Create your models here.
