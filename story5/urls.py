from django.urls import path
from . import views


app_name = 'story5'

urlpatterns = [
    path('story5/' ,views.createMatkul, name='createMatkul'),
    path('createMatkul/' ,views.createMatkul, name='createMatkul'),
    path('lihatMatkul/' ,views.lihatMatkul, name='lihatMatkul'),
    path('detail/<str:cek_id>/' ,views.detail, name='detail'),
    path('delete/<int:delete_id>/', views.deleteMatkul, name='delete'),
    
    
]