from django.db import models
# Create your models here.
from django.forms import ModelForm

class modelKegiatan(models.Model):
    namaKegiatan = models.CharField(max_length=100)
    
    def __str__(self):
		    return "{}".format(self.namaKegiatan)

class modelPeserta(models.Model) :
    namaPeserta = models.CharField(max_length=100)
    kegiatan = models.ForeignKey(modelKegiatan, on_delete= models.CASCADE)
    
    def __str__(self):
		    return "{}".format(self.namaPeserta)

# Create your models here.

	