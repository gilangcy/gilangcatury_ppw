from django.test import TestCase

# Create your tests here.

from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import *
from .models import *
from .forms import *
import time
# Create your tests here.
class Lab6UnitTest(TestCase):
    def test_path_createKegiatan(self):
        response = Client().get('/createKegiatan')
        self.assertEquals(response.status_code,200)
    def test_path_index(self):
        response = Client().get('/story6/')
        self.assertEquals(response.status_code,200)
    def test_path_createPeserta(self):
        new_model = modelKegiatan.objects.create(namaKegiatan ='sabeb')
        response = Client().get('/createPeserta/1/')
        self.assertEquals(response.status_code,200)

    def test_input_createMatkul(self):
        response = Client().get('/createKegiatan')
        html = response.content.decode('utf-8')
        self.assertIn('Halo Semua',html)
    def test_input_createPeserta(self):
        new_model = modelKegiatan.objects.create(namaKegiatan ='sabeb')
        response = Client().get('/createPeserta/1/')
        html = response.content.decode('utf-8')
        self.assertIn('List Peserta',html)

    def test_url_using_func(self):
        found = resolve('/createKegiatan')
        self.assertEqual(found.func, createKegiatan)
    def test_func_createPeserta(self):
        new_model = modelKegiatan.objects.create(namaKegiatan ='sabeb')
        found = resolve('/createPeserta/1/')
        self.assertEqual(found.func, createPeserta)

    def test_create_models(self):
        new_model = modelKegiatan.objects.create(namaKegiatan ='sabeb')
        counting_new_model = modelKegiatan.objects.all().count()
        self.assertEqual(counting_new_model,1)
    def test_create_peserta(self):
        new_model = modelKegiatan.objects.create(namaKegiatan ='sabeb')
        new_peserta = modelPeserta.objects.create(namaPeserta ='sabebs', kegiatan=new_model)
        counting_new_peserta = modelPeserta.objects.all().count()
        self.assertEqual(counting_new_peserta,1)

    def test_form_is_blank(self):
        form = formKegiatan(data= {'namaKegiatan':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['namaKegiatan'],
            ["This field is required."]
            )
    def test_form_template_used(self):
        response = Client().get('/story6/')
        self.assertTemplateUsed(response, 'page.html')

    def test_post_form(self):
        response = Client().post('/story6/',{
				'namaKegiatan':'hai'
			})
        self.assertIn('hai',response.content.decode())
    
    
