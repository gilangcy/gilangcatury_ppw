from .models import modelKegiatan, modelPeserta
from django import forms


class formPeserta(forms.ModelForm):
    class Meta:

        model = modelPeserta
        fields = ('namaPeserta',)


class formKegiatan(forms.ModelForm):
    class Meta:

        model = modelKegiatan
        fields = ('namaKegiatan',)
