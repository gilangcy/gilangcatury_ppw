from django.shortcuts import render
from .models import modelKegiatan, modelPeserta
from .forms import formKegiatan, formPeserta

# Create your views here.

def createKegiatan(request):
    listKegiatan = modelKegiatan.objects.all()
    kegiatan = formKegiatan(request.POST or None)
    peserta = formPeserta(request.POST or None)
    context = {
            'list_kegiatan' : listKegiatan,
            'kegiatan': kegiatan, 'peserta':peserta
    }
    if request.method == 'POST' :
        if kegiatan.is_valid() :
            listKegiatan.create(
                namaKegiatan = kegiatan.cleaned_data.get('namaKegiatan'),
            )
    return render(request,'page.html',context)
    
def createPeserta(request, kegiatan_id):
    listPeserta = modelPeserta.objects.all()
    listKegiatan = modelKegiatan.objects.get(id=kegiatan_id)
    peserta = formPeserta(request.POST or None)
    context = {
        'listKegiatan':listKegiatan,
        'peserta': peserta,
        'list_peserta' : listPeserta
    }
    if request.method == 'POST' :
        if peserta.is_valid() :
            listPeserta.create(
                namaPeserta = peserta.cleaned_data.get('namaPeserta'),
                kegiatan = listKegiatan
            )
    return render (request,"buatPeserta.html",context)