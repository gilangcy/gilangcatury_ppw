from django.urls import path
from . import views


app_name = 'story6'

urlpatterns = [
    path('story6/',views.createKegiatan,name = 'homepage'),
    path('createKegiatan', views.createKegiatan, name='createKegiatan'),
    path('createPeserta/<int:kegiatan_id>/', views.createPeserta, name='createPeserta'),
    
    
]