from django.urls import path
from . import views

app_name = 'story9'

urlpatterns = [
    path('',views.loginstory9,name = 'story9'),
    path('story9/',views.loginstory9,name = 'story9'),
    path('halaman/',views.story9,name='halaman'),
    path('logout/',views.logoutstory,name='logoutstory'),
    path('register/',views.register,name='register')
]