from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import *
from .models import *

# Create your tests here.
class story9_testPath(TestCase):
    def test_path_story9(self):
        response = Client().get('/story9/',{},True)
        self.assertEquals(response.status_code,200)
    def test_path_halaman(self):
        response = Client().get('/halaman/',{},True)
        self.assertEquals(response.status_code,200)
    def test_path_logout(self):
        response = Client().get('/logout/',{},True)
        self.assertEquals(response.status_code,200)
    def test_path_register(self):
        response = Client().get('/register/',{},True)
        self.assertEquals(response.status_code,200)



class story9_testInput(TestCase):
    def test_input_story9(self):
        response = Client().get('/story9/')
        html = response.content.decode('utf-8')
        self.assertIn('Username',html)
        self.assertIn('Password',html)
       
    
  
class story9_testFunction(TestCase):
    def test_url_using_func11(self):
        found = resolve('/story9/')
        self.assertEqual(found.func, loginstory9)
    def test_url_using_func12(self):
        found = resolve('/halaman/')
        self.assertEqual(found.func, story9)
    def test_url_using_func13(self):
        found = resolve('/logout/')
        self.assertEqual(found.func, logoutstory)
    def test_url_using_func12(self):
        found = resolve('/register/')
        self.assertEqual(found.func, register)


class story9_testTemplate(TestCase):
    def test_form_template_used1(self):
        response = Client().get('/story9/')
        self.assertTemplateUsed(response, 'story9.html')
    
class story9_testForm(TestCase):
    def test_post_form1(self):
        response = Client().post('/story9/',{
				'username':'iniusername','password':'inipass'
			})
    def test_post_form2(self):
            response = Client().post('/register/',{
                    'username':'iniusername','password1':'inipass','first_name':'inifirstname','last_name':'inilastname'
                })
    
  
    