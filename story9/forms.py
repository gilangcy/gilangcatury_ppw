from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

class formUsers(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username','password')
        labels = {'username':'Username :','password':'Password :'}
        


class formUser(UserCreationForm):
    password1 = forms.CharField(label='Enter password :', 
                                widget=forms.PasswordInput(attrs={'class':'form-control','placeholder': "Put your password here",  "autocomplete":"off"}))
    password2 = forms.CharField(label='Confirm password :', 
                                widget=forms.PasswordInput(attrs={'class':'form-control','placeholder': "Confirm your password here",  "autocomplete":"off"}))
    class Meta:
        model=User
        fields=("username","first_name",
                "last_name","password1","password2",)
        labels ={'username':'Username :',"first_name":'First Name :',
                "last_name":'Last Name :',}
        help_texts = {
            "username":None,
        }
        widgets ={
            'username': forms.TextInput(attrs={'class':'form-control','placeholder': "Put your username here",  "autocomplete":"off"}),
            'first_name': forms.TextInput(attrs={'class':'form-control','placeholder': "Put your First name here",  "autocomplete":"off"}),
            'last_name': forms.TextInput(attrs={'class':'form-control','placeholder': "Put your Last name here",  "autocomplete":"off"}),
            }
        
        
    
        
        