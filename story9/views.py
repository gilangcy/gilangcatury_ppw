from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, User
from django.contrib.auth import authenticate, login ,logout
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from .forms import formUser

# Create your views here.
def story9(request):
    if User.is_authenticated:
        return render(request,'halaman.html')
    else :
        return redirect('story9:story9')

def logoutstory(request):
    return redirect('story9:story9')

def loginstory9(request):
    logout(request)
    context = {
		'page_title':'LOGIN',
	}
    user = None
    if request.method == "POST":
        username_login = request.POST['username']
        password_login = request.POST['password']
        user = authenticate(request, username=username_login, password=password_login)
        if user is not None:
            login(request,user)
            return redirect('story9:halaman')
           
        else:
            messages.warning(request, f'Username atau Password anda salah!')
            return redirect('story9:story9')
           
		
    return render(request, 'story9.html', context)

def register(request):
    form = formUser(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            messages.success(request, f'Akun anda berhasil dibuat')
            return redirect('story9:story9')
    return render(request,'register.html',{'form_user':form})

 