from django.shortcuts import render
from django.http import JsonResponse
from django.http import HttpResponse
import json
import requests


# Create your views here.
def book(request):
    return render (request,'book.html')

def googleapis(request):
    arg = request.GET['q']
    url = "https://www.googleapis.com/books/v1/volumes?q="+ arg
    retrieve = requests.get(url)
    data = json.loads(retrieve.content)
    return JsonResponse(data,safe=False)