from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import *
from .models import *

# Create your tests here.
class story8_testPath(TestCase):
    def test_path_story8(self):
        response = Client().get('/story8/',{},True)
        self.assertEquals(response.status_code,200)
    def test_path(self):
        response = Client().get('/data?q=',{},True)
        self.assertEquals(response.status_code,200)

class story8_testInput(TestCase):
    def test_input_story8(self):
        response = Client().get('/story8/')
        html = response.content.decode('utf-8')
        self.assertIn('Cari Buku Yuk!',html)
       
    
  
class story8_testFunction(TestCase):
    def test_url_using_func(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, book)
    def test_url_using_func2(self):
        found = resolve('/data/')
        self.assertEqual(found.func, googleapis)


class story8_testTemplate(TestCase):
    def test_form_template_used1(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'book.html')
  
    