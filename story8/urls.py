from django.urls import path
from . import views

app_name = 'story8'

urlpatterns = [
    path('story8/',views.book,name = 'book'),
    path('data/',views.googleapis,name='getData')
    
]